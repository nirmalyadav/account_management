# rubocop:disable Style/FrozenStringLiteralComment
# rubocop:enable Style/FrozenStringLiteralComment
# Handles CRUD Operations of Students using ActiveInteraction.
class StudentsController < ApplicationController
  # Renders list of students.
  #
  # @accepts HTML
  # @verb GET
  def index
    @students = ListStudents.run!
    respond_to do |format|
      format.html {}
      format.csv do
        send_data CsvExportWorker.perform_async('student')
      end
    end
  end

  # Build and renders form for new <tt>Student</tt>.
  #
  # @accepts HTML
  # @verb GET
  def new
    @student = CreateStudent.new
  end

  # Creates new <tt>Student</tt> using params.
  #
  # @post_params :student [Hash]
  #   * first_name [String]
  #   * last_name [String]
  #   * course_name [String]
  #   * age [String]
  #
  # @accepts HTML
  # @verb POST
  def create
    @student = CreateStudent.run(student_params)

    if @student.valid?
      flash[:success] = t('controllers.students.flash.success.create_success')
      redirect_to(@student.result)
    else
      render :new
    end
  end

  # Renders for to edit <tt>Student</tt>
  #
  # @get_params :id [String] Student#id
  #
  # @accepts HTML
  # @verb GET
  def edit
    @student = Student.find(params[:id])
  end

  # Renders <tt>Student</tt> details.
  #
  # @get_params :id [String] Student#id
  #
  # @accepts HTML
  # @verb GET
  def show
    @student = find_student!
  end

  # Updates <tt>Student</tt> details.
  #
  # @get_params :id [String] Student#id
  # @post_params :student [Hash]
  #   * first_name [String]
  #   * last_name [String]
  #   * course_name [String]
  #   * age [String]
  #
  # @accepts HTML
  # @verb PATCH
  def update
    inputs = { student: find_student! }.reverse_merge(student_params)
    @student = UpdateStudent.run(inputs)

    if @student.valid?
      flash[:success] = t('controllers.students.flash.success.update_success')
      redirect_to(@student.result)
    else
      render(:edit)
    end
  end

  private

  # Finds <tt>Student</tt>
  #
  # @return [Student]
  def find_student!
    outcome = FindStudent.run(params)
    outcome.result
  end

  # Whitelists permitted params (attributes) of Students.
  #
  # @return [Object<ActionController::Parameters>]
  def student_params
    params.require(:student).permit(:first_name, :last_name, :course_name, :age, :gender)
  end
end
