ActiveAdmin.register Student do

  permit_params :first_name, :last_name, :course_name, :age, :gender

  index do
    selectable_column
    id_column
    column :first_name
    column :last_name
    column :course_name
    column :age
    column :gender
    column :created_at
    actions
  end

  filter :first_name
  filter :last_name
  filter :course_name
  filter :age
  filter :gender

  form do |f|
    f.inputs do
      f.input :first_name
      f.input :last_name
      f.input :course_name
      f.input :age
      f.input :gender
    end
    f.actions
  end

end
