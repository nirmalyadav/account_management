class CsvExportWorker
  include Sidekiq::Worker

  def perform(model_name)
    model = model_name.classify.constantize
    model.to_csv  
  end
end
