class FindStudent < ActiveInteraction::Base

  integer :id

  # Finds Student using ActiveInteraction
  def execute
    Student.find(id)
  end

end