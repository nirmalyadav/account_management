class CreateStudent < ActiveInteraction::Base

  string :first_name
  string :last_name
  string :course_name
  integer :age
  integer :gender

  validates :first_name, :last_name, :course_name, :age, presence: true
  validates :age, numericality: true

  def to_model
    Student.new
  end

  # Creates Student using ActiveInteraction
  def execute
    Student.create!(inputs.slice(:first_name, :last_name, :course_name, :age, :gender))
  end

end