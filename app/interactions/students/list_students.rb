require 'active_interaction'

class ListStudents < ActiveInteraction::Base

  # Fetches all the Students ordered by first_name in ascending order.
  def execute
    Student.order(first_name: :asc)
  end

end