class UpdateStudent < ActiveInteraction::Base
  object :student

  string :first_name
  string :last_name
  string :course_name
  integer :age
  integer :gender

  validates :first_name, :last_name, :course_name, presence: true
  validates :age, presence: true, numericality: true

  # Updates Student details.
  def execute
    student.first_name = first_name
    student.last_name = last_name
    student.course_name = course_name
    student.age = age
    student.gender = gender
    unless student.save
      errors.merge!(student.errors)
    end

    student
  end
end