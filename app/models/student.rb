class Student < ApplicationRecord

  require 'csv'

  enum gender: %i[male female]

  # Full Name of Student
  #
  # @return [String]
  def name
    "#{first_name} #{last_name}"
  end

  # Generates CSV files for Students.
  #
  # @return [void]
  def self.to_csv
    CSV.generate(headers: true) do |csv|
      csv << column_names
      all.each do |name|
        csv << name.attributes.values_at(*column_names)
      end
    end
  end

end
