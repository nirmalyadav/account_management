class Ability

  include CanCan::Ability

  def initialize(user)
    return unless user

    if user.super_admin?
      can :manage, :all
    else
      can :manage, AdminUser, id: user.id
      can :read, :all
    end
  end

end
