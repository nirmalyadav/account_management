Rails.application.routes.draw do

  root 'homes#index'
  get 'chat_room', to: 'homes#chat_room'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users, controllers: { sessions: 'users/sessions' }

  resources :students

end
