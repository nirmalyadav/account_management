class AddRoleToAdminUser < ActiveRecord::Migration[6.1]
  def change
    add_reference :admin_users, :role
  end
end
